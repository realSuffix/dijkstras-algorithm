use crate::node::Node;
use std::cmp::Ordering;
use std::collections::{HashMap, HashSet};
use std::hash::Hash;
use std::rc::Rc;

#[derive(Debug, PartialEq, Eq, Hash)]
/// The first member is the smallest distance beginning from the start-vertex,
/// the second member is the previous vertex
pub struct KnownDistance<T: Hash + Eq + PartialEq>(pub i32, pub Option<Rc<Node<T>>>);

impl<T> KnownDistance<T>
where
    T: Hash + Eq,
{
    pub fn new(cost: i32, prev: Option<Rc<Node<T>>>) -> Self {
        Self(cost, prev)
    }
}

impl<T> Ord for KnownDistance<T>
where
    T: Hash + Eq + PartialEq,
{
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.cmp(&other.0)
    }
}

impl<T> PartialOrd for KnownDistance<T>
where
    T: Hash + Eq + PartialEq,
{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Debug)]
pub struct DijkstraResult<T>
where
    T: Hash + Eq,
{
    pub entries: HashMap<Rc<Node<T>>, KnownDistance<T>>,
}

impl<T> DijkstraResult<T>
where
    T: Hash + Eq,
{
    pub fn new() -> Self {
        Self {
            entries: HashMap::new(),
        }
    }

    pub fn add_entry(&mut self, node: Rc<Node<T>>) {
        self.entries
            .insert(node, KnownDistance::new(i32::MAX, None));
    }

    /// This function also verifies whether the entry needs to be updated in the first place
    pub fn update_entry(
        &mut self,
        to_update: Rc<Node<T>>,
        cost: i32,
        new_parent: Option<Rc<Node<T>>>,
    ) {
        let KnownDistance(old_cost, ..) = self.entries.get(&to_update).unwrap();

        if cost < *old_cost {
            self.entries
                .insert(to_update, KnownDistance::new(cost, new_parent));
        }
    }

    pub fn get_lowest_dist_vert(&self, unvisited: &HashSet<Rc<Node<T>>>) -> Option<(Rc<Node<T>>, i32)> {
        self.entries
            .iter()
            .filter(|(key, _)| unvisited.contains(*key)) // make sure that we only search within the unvisited ones
            .min_by_key(|(_, value)| value.0) // value.0 is the cost!
            .map(|(node, value)| (Rc::clone(node), value.0)) // we need to map since we don't need a tuple but just the key
    }
}
