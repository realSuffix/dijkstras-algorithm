use crate::adjacency_matrix::*;
use crate::dijkstra_result::*;
use crate::node::*;
use std::cell::RefCell;
use std::collections::HashSet;
use std::hash::Hash;
use std::rc::Rc;

#[derive(Debug)]
pub struct Graph<T>
where
    T: Hash + Eq,
{
    /// Represents the graph
    matrix: AdjacencyMatrix<T>,

    /// The table which is generated
    table: RefCell<DijkstraResult<T>>,

    /// The unvisited nodes
    unvisited: HashSet<Rc<Node<T>>>,
}

impl<T> Graph<T>
where
    T: Hash + Eq + std::fmt::Debug,
{
    pub fn new() -> Self {
        Self {
            matrix: AdjacencyMatrix::new(),
            table: RefCell::new(DijkstraResult::new()),
            unvisited: HashSet::new(),
        }
    }

    pub fn add_adjacency(&mut self, from: Rc<Node<T>>, to: Rc<Node<T>>, cost: i32) {
        self.matrix
            .add_adjacency(Rc::clone(&from), Rc::clone(&to), cost);

        // We insert from & to respectively, to make sure that all
        // vertices are contained
        self.table.borrow_mut().add_entry(Rc::clone(&from));
        self.table.borrow_mut().add_entry(Rc::clone(&to));

        self.unvisited.insert(from);
        self.unvisited.insert(to);
    }

    pub fn dijkstra(&mut self, start: Rc<Node<T>>) {
        // The entries for all the other vertices are set to max,
        // here we want to set it to zero as its the starting point
        self.table
            .borrow_mut()
            .update_entry(Rc::clone(&start), 0, None);

        // We need it in a seperate variable so that we avoid borrowing in the loop
        let mut is_done = self.unvisited.is_empty();

        while !is_done {
            // get vertex with lowest cost
            let lowest_vert = self
                .table
                .borrow()
                .get_lowest_dist_vert(&self.unvisited)
                .unwrap();

            // get all neighbours
            let neighbours = self.matrix.get_neighbours_for_node(&lowest_vert.0).unwrap();

            neighbours
                .iter()
                .filter(|(node_ref, _)| self.unvisited.contains(node_ref)) // only take a look at the unvisited ones
                .for_each(|(node_ref, cost)| {
                    // lowest_vert.1 is the cost we had up until now,
                    // cost is the cost which is added due to the connection
                    // to the new neighbour
                    let new_cost = lowest_vert.1 + cost;
                    self.table.borrow_mut().update_entry(
                        Rc::clone(node_ref),
                        new_cost,
                        Some(Rc::clone(&lowest_vert.0)),
                    );
                });

            // Visit the current lowerst vertex
            // by removing it from the list of the unvisited
            // ones
            self.unvisited.remove(&lowest_vert.0);

            is_done = self.unvisited.is_empty();
        }

        println!("{:#?}", self.table.borrow());
    }
}
