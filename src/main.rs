mod adjacency_matrix;
mod dijkstra_result;
mod graph;
mod io_handler;
mod node;

use crate::io_handler::read_input;
use std::rc::Rc;

fn main() {
    let (mut graph, nodes) = read_input();
    graph.dijkstra(Rc::clone(&nodes[0]));
}
